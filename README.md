# devops-netology

### первое изменение

### Гитигнор терраформ. Будут проигнорированы:
1. все файлы, содержащие в имени ".tfstate."
1. файлы crash.log, override.tf, override.tf.json, .terraformrc, terraform.rc
1. файлы с расширением ".tfvars"
1. файлы, оканчивающиеся на "_override.tf" и "_override.tf.json"
